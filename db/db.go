package db

import (
	"database/sql"

	//import dialect
	_ "github.com/go-sql-driver/mysql"

	"github.com/spf13/viper"
)

var db *sql.DB
var err error

// InitMysql for connect to mysql
func InitMysql() (*sql.DB, error) {
	//open a mysql connection
	db, err = sql.Open("mysql", viper.GetString("database.mysql.user")+":"+viper.GetString("database.mysql.password")+"@tcp("+viper.GetString("database.mysql.host")+":"+viper.GetString("database.mysql.port")+")/"+viper.GetString("database.mysql.dbname")+"?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		return nil, err
	}

	return db, nil
}

// GetDB is for getting current db connection
func GetDB() *sql.DB {
	return db
}
