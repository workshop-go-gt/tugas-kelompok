package controllers

import (
	"strconv"
	"tugas-kelompok/forms"
	"tugas-kelompok/models"

	"github.com/gin-gonic/gin"
)

// UserController is
type UserController struct{}

var personModel = new(models.PersonModel)

// List is
func (s *UserController) List(c *gin.Context) {
	personList := personModel.GetList()
	c.JSON(200, gin.H{
		"status":  200,
		"message": "success",
		"data": gin.H{
			"users": personList,
		},
	})
}

// Create is
func (s *UserController) Create(c *gin.Context) {
	code := 200
	msg := "success"
	var params forms.Person
	params.FirstName = c.PostForm("firstname")
	params.LastName = c.PostForm("lastname")
	params.Phone = c.PostForm("phone")
	params.Email = c.PostForm("email")
	err := personModel.Add(params)
	if err != nil {
		code = 406
		msg = err.Error()
	}
	c.JSON(code, gin.H{
		"status":  code,
		"message": msg,
	})
}

// GetByID is
func (s *UserController) GetByID(c *gin.Context) {
	personID, _ := strconv.Atoi(c.Param("id"))
	person := personModel.GetOne(personID)
	c.JSON(200, gin.H{
		"status":  200,
		"message": "success",
		"data":    person,
	})
}

// Update is
func (s *UserController) Update(c *gin.Context) {
	code := 200
	msg := "success"
	personID, _ := strconv.Atoi(c.Param("id"))
	var params forms.Person
	params.FirstName = c.PostForm("firstname")
	params.LastName = c.PostForm("lastname")
	params.Phone = c.PostForm("phone")
	params.Email = c.PostForm("email")
	err := personModel.Update(params, personID)
	if err != nil {
		code = 406
		msg = err.Error()
	}
	c.JSON(code, gin.H{
		"status":  code,
		"message": msg,
	})
}

// DeleteOne is
func (s *UserController) DeleteOne(c *gin.Context) {
	code := 200
	msg := "success"
	personID, _ := strconv.Atoi(c.Param("id"))
	err := personModel.RemoveOne(personID)
	if err != nil {
		code = 406
		msg = err.Error()
	}
	c.JSON(code, gin.H{
		"status":  code,
		"message": msg,
	})
}
