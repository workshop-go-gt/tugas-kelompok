CREATE TABLE persons (
	id INT(11) NOT NULL AUTO_INCREMENT,
	firstname varchar(200) NULL,
    lastname varchar(200) NULL,
    phone varchar(200) NULL,
    email varchar(200) NULL,
    PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;