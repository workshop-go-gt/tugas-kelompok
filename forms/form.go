package forms

// Person is the data structure for persons table
type Person struct {
	ID        int    `json:"id"`
	FirstName string `faker:"first_name" json:"firstname"`
	LastName  string `faker:"last_name" json:"lastname"`
	Phone     string `faker:"phone_number" json:"phone"`
	Email     string `faker:"email" json:"email"`
}
