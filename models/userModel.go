package models

import (
	"tugas-kelompok/db"
	"tugas-kelompok/forms"
)

//PersonModel is the user model
type PersonModel struct{}

// GetList is for getting list of persons
func (um *PersonModel) GetList() (p []forms.Person) {
	rows, err := db.GetDB().Query("select id, firstname, lastname, phone, email from persons")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		var person forms.Person
		err = rows.Scan(&person.ID, &person.FirstName, &person.LastName, &person.Phone, &person.Email)
		if err != nil {
			panic(err)
		}
		p = append(p, person)
	}
	err = rows.Err()
	if err != nil {
		panic(err)
	}
	return
}

// Add is for inserting new person to table
func (um *PersonModel) Add(data forms.Person) error {
	_, err := db.GetDB().Exec("insert into persons (firstname, lastname, phone, email) values (?, ?, ?, ?)", data.FirstName, data.LastName, data.Phone, data.Email)
	return err
}

// GetOne is for getting one of person
func (um *PersonModel) GetOne(ID int) (p forms.Person) {
	rows, err := db.GetDB().Query("select firstname, lastname, phone, email from persons where id = ? ", ID)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	rows.Next()
	err = rows.Scan(&p.FirstName, &p.LastName, &p.Phone, &p.Email)
	if err != nil {
		panic(err)
	}
	return
}

// Update is for updating person data
func (um *PersonModel) Update(data forms.Person, ID int) error {
	_, err := db.GetDB().Exec("update persons set firstname = ? , lastname = ?, phone = ?, email = ? where id = ?", data.FirstName, data.LastName, data.Phone, data.Email, ID)
	return err
}

// RemoveOne is for updating person data
func (um *PersonModel) RemoveOne(ID int) error {
	_, err := db.GetDB().Exec("delete from persons where id = ?", ID)
	return err
}
