package routes

import (
	"tugas-kelompok/controllers"

	"github.com/gin-gonic/gin"
)

//DefaultRoute is
type DefaultRoute struct{}

//Init is
func (d *DefaultRoute) Init(router *gin.Engine) {
	uc := new(controllers.UserController)
	router.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "hello there",
		})
	})
	v1 := router.Group("/v1")
	{
		v1.GET("/person", uc.List)
		v1.POST("/person", uc.Create)
		v1.GET("/person/:id", uc.GetByID)
		v1.PATCH("/person/:id", uc.Update)
		v1.DELETE("/person/:id", uc.DeleteOne)
	}
	router.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{
			"status":  404,
			"message": "url not found",
		})
	})
}
