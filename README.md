# Tugas Kelompok (Backend)

Just a simple API written in Go with Migration

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

**!! WARNING !!** This project is using Go [Modules](https://blog.golang.org/using-go-modules) which is the minimum version of installed golang is 1.11. 

### Installing

A step by step series of examples that tell you how to get a development env running
1. Clone this project `git clone https://gitlab.com/workshop-go-gt/tugas-kelompok.git` outside $GOPATH (for version >= 1.11) or inside $GOPATH/src (for version < 1.11) 
2. Setting up your host, port, and database configuration in `config/config.json`
3. Create your Mysql Database based on your config
4. **(for version under 1.11)** You need to install following package first :
```
go get -u github.com/gin-gonic/gin
go get -u github.com/spf13/viper
go get -u github.com/bxcodec/faker/v3
go get -u github.com/golang-migrate/migrate
go get -u github.com/go-sql-driver/mysql
```
5. The available **mode** is **development** or **production**. Change to production for a live system
6. Just run `go run main.go`
7. Wait a sec, the system will preparing your app including **migration** and **generating fake data** (if the environtment is development)
Read more about [migration](https://github.com/golang-migrate/migrate)

### Available Endpoint

- [GET]     /
- [GET]     /v1/person
- [POST]    /v1/person
- [GET]     /v1/person/:id
- [PATCH]   /v1/person/:id 
- [DELETE]  /v1/person/:id
- [complete_docs](https://documenter.getpostman.com/view/2710620/SVYxoFMr)

## Built With

* [Gin](https://github.com/gin-gonic/gin)
* [Viper](https://github.com/spf13/viper)
* [Faker](https://github.com/bxcodec/faker)
* [Migration](https://github.com/golang-migrate/migrate)
* [MysqlDriver](https://github.com/go-sql-driver/mysql)