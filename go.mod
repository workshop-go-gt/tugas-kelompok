module tugas-kelompok

go 1.12

require (
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/gin-gonic/gin v1.4.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/jinzhu/gorm v1.9.10
	github.com/spf13/viper v1.4.0
)
