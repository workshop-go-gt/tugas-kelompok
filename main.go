package main

import (
	"fmt"
	"log"

	"tugas-kelompok/db"
	"tugas-kelompok/forms"
	"tugas-kelompok/routes"

	"github.com/bxcodec/faker"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigFile("config.json")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}
}

func main() {
	dbConn, err := db.InitMysql()
	err = dbConn.Ping()
	if err != nil {
		panic(fmt.Errorf("Error ping : %s", err))
	}
	defer dbConn.Close()
	_, tblErr := dbConn.Query("select id from persons;")
	if tblErr != nil {
		log.Println("Starting migration")
		driver, err := mysql.WithInstance(dbConn, &mysql.Config{})
		if err != nil {
			log.Println(err.Error())
			log.Println("Migration canceled")
		} else {
			m, err := migrate.NewWithDatabaseInstance(
				"file://migrations",
				"mysql", driver)
			if err != nil {
				log.Println(err.Error())
				log.Println("Migration canceled")
			} else {
				err = m.Steps(1)
				if err != nil {
					log.Println(err.Error())
					log.Println("Migration canceled")
				} else {
					log.Println("Migration success")
					if viper.GetString("mode") != "production" {
						log.Println("Generating fake data ...")
						for i := 0; i < 3; i++ {
							var a forms.Person
							err := faker.FakeData(&a)
							if err != nil {
								log.Println(err)
							}
							db.GetDB().Exec("insert into persons (firstname, lastname, phone, email) values (?, ?, ?, ?)", a.FirstName, a.LastName, a.Phone, a.Email)
						}
						log.Println("Fake data generated")
					}
				}
			}
		}

	}
	defaultRoute := new(routes.DefaultRoute)
	r := gin.Default()
	defaultRoute.Init(r)
	if viper.GetString("mode") == "production" {
		gin.SetMode(gin.ReleaseMode)
	}
	r.Run(viper.GetString("server.host") + ":" + viper.GetString("server.port"))
}
